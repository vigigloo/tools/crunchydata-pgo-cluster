# PostgreSQL Operator Cluster

## Backups

This is a quick overview of how backups work using PGO.
More extensive documentation can be found on CrunchyData's documentation:
- [Backup Configuration](https://access.crunchydata.com/documentation/postgres-operator/5.0.5/tutorial/backups/)
- [Backup Management](https://access.crunchydata.com/documentation/postgres-operator/5.0.5/tutorial/backup-management/)
- [Disaster Recovery and Cloning](https://access.crunchydata.com/documentation/postgres-operator/5.0.5/tutorial/disaster-recovery/)

### Listing

Unless you configured the backups to be made to a cloud storage solution, they will be persisted on a volume mounted by a pod that acts as a repo host.
This pod can thus be used to list the available backups.

```bash
kubectl exec <repohostpod> -- ls /pgbackrest/repo1/backup/db
```

### Manual

To perform a manual backup, the `PostgresCluster` resource needs to be edited.

```bash
# configure the backup options
export KUBECONFIG=~/.kube/config
export KUBECTL_NAMESPACE=default
export KUBECTL_CONTEXT= # optionally specify the context if needed
export PG_BACKUP_CLUSTER_NAME=postgresql
export PG_BACKUP_REPO=repo1
export PG_BACKUP_TYPE=full #full, diff or incr

# generate patch files
./backup-genpatch.sh

# apply patch that triggers the manual backup
KUBECTL_CONTEXT_FLAG=$(if [ ! -z $KUBECTL_CONTEXT ]; then echo "--context $KUBECTL_CONTEXT"; fi)
kubectl -n $KUBECTL_NAMESPACE $KUBECTL_CONTEXT_FLAG patch postgrescluster $PG_BACKUP_CLUSTER_NAME --type merge --patch-file backup-patch.yaml
```

Once the backup is completed, these edits can be erased from the `PostgresCluster`.

```bash
# apply patch that removes the manual backup from the resource spec
kubectl -n $KUBECTL_NAMESPACE $KUBECTL_CONTEXT_FLAG patch postgrescluster $PG_BACKUP_CLUSTER_NAME --type merge --patch-file backup-patch-end.yaml
```

### Restore

There are two ways to restore a backup.
You can perform a Point-in-time-recovery, or restore from a backup set.
Point-in-time-recovery allows restoring the database to the state at an exact date of your choice.
Restoring from a backup set instead restores to the date the backup was taken.

If a backup was taken and while no new transactions were recorded, a PITR restore will fail.
In this case, restore from a backup set instead.

```bash
# configure the restoration options
export KUBECONFIG=~/.kube/config
export KUBECTL_NAMESPACE=default
export KUBECTL_CONTEXT= # optionally specify the context if needed
export PG_RESTORE_CLUSTER_NAME=postgresql
export PG_RESTORE_REPO=repo1

# use either one below
# restoring using pitr allows for an exact restoration date
export PG_RESTORE_DATE="2022-08-11 09:49:00+02"
# restoring using a set requires the exact set name
# export PG_RESTORE_SET="20220811-091116F"

# generate patch files
./restore-genpatch.sh

KUBECTL_CONTEXT_FLAG=$(if [ ! -z $KUBECTL_CONTEXT ]; then echo "--context $KUBECTL_CONTEXT"; fi)
kubectl -n $KUBECTL_NAMESPACE $KUBECTL_CONTEXT_FLAG patch postgrescluster $PG_RESTORE_CLUSTER_NAME --type merge --patch-file restore-patch.yaml
```

Once the backup is restored, these edits can be erased from the `PostgresCluster`.

```bash
# After the restore is completed
kubectl -n $KUBECTL_NAMESPACE $KUBECTL_CONTEXT_FLAG patch postgrescluster $PG_RESTORE_CLUSTER_NAME --type merge --patch-file restore-patch-end.yaml
```

## Major upgrades

### Upgrading by migrating to a new cluster

This procedure, while requiring a lot of manual adjustments and tinkering, is the most versatile when it comes to version deltas. It is [recommended when upgrading clusters that use PostGIS](https://postgis.net/workshops/postgis-intro/upgrades.html#dump-restore).

The high-level steps are as follows:
- Create a new cluster. It can have only one replica to simplify the upgrade procedure.
- Stop any workload that uses the database with `kubectl scale <deploy/sts> <name> --replicas 0`
- Get the primary Pod's name with `kubectl get pod -o name --selector=postgres-operator.crunchydata.com/role=master`. It should return the primaries for both the new and the old clusters.
- Dump the existing database with `kubectl exec -t <primary pod> -- pg_dump -Z 2 <dbname> | gzip -d > dump.sql`. This command uses compression to achieve higher throughput.
- You'll most likely need to tweak the dump to fit your new cluster
  - If the name of the database changes, edit the dump accordingly with `sed -i 's/OWNER TO <old dbname>;$/OWNER TO <new dbname>;/' dump.sql`
  - When using PostGIS, if the new cluster already has the `tiger`, `tiger_data` and `topology` schemas, adjust the `CREATE SCHEMA` statements with `sed -i 's/^CREATE SCHEMA /CREATE SCHEMA IF NOT EXISTS /' dump.sql`.
- Restore the dump to the new cluster with `kubectl exec -i <new primary pod> -- psql <new dbname> -v ON_ERROR_STOP=1 < dump.sql`
  - If an error occured and the new cluster needs to be wiped clean, use `kubectl exec -i <new primary pod> -- dropdb <new dbname> ; kubectl exec -i <new primary pod> -- createdb <new dbname>`
- Add `spec.shutdown: true` to the old cluster
- Redeploy the workloads that use the database with the new host, user and password.
- When everything works fine, the old cluster can be deleted.

### Upgrading using PGUpgrade

This is a summary of the upgrade procedure described in [the official documentation](https://access.crunchydata.com/documentation/postgres-operator/5.3.1/guides/major-postgres-version-upgrade/). Refer to it for more details.

The high-level steps are as follows:
- Stop any workload that uses the database with `kubectl scale <deploy/sts> <name> --replicas 0`
- Create the pgupgrade object with `kubectl apply -f pgupgrade.yaml`
- Edit the cluster to set `spec.shutdown: true` and the annotation `postgres-operator.crunchydata.com/allow-upgrade: <pgupgrade name>`
- Wait for the upgrade to finish. Inspect the state of the `pgupgrade` object with `kubectl describe pgupgrade <pgupgrade name>`
- Run a helm release to set `spec.postgresVersion`, `spec.image` and `spec.backups.pgbackrest.image` to the new version and remove the previous edits. If the operator has the correct related images configured it will use the correct image by itself and the image fields can be left to `null`
- Once the cluster has restarted, check for post-upgrade scripts to run under `/pgdata` on the primary
- Restart the workloads that uses the database with `kubectl scale <deploy/sts> <name> --replicas <number>`

```yaml
# pgupgrade.yaml
apiVersion: postgres-operator.crunchydata.com/v1beta1
kind: PGUpgrade
metadata:
  namespace: <namespace>
  name: <pgupgrade name>
spec:
  postgresClusterName: <cluster name>
  fromPostgresVersion: <current version>
  toPostgresVersion: <new version>
  resources:
    limits:
      cpu: "1"
      memory: 1Gi
```

## Mirroring images in another registry

CrunchyData frequently closes access to older container images.
In order to prevent breaking deployments when they need to pull images again, it is advised to push the images to a dedicated container registry.

First, the list of images that can be useful to mirror can be found in their [operator helm chart](https://github.com/CrunchyData/postgres-operator-examples)'s values.
Instead of only showing the images for the latest version, we can also list the images for all commits:
```
git log --format=format:%H values.yaml | xargs -n 1 -I{} git show {}:helm/install/values.yaml | grep registry | sed 's/.*: //' | sort | uniq | grep ':' | grep developers > images.txt
```

Then, the idea is just to `docker pull <theirs> ; docker tag <theirs> <ours> ; docker push <ours>`.
There is a script that does that with a little more check that can be run to update a registry with the latest images more efficiently called `update_pgo_images.sh`.

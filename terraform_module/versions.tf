terraform {
  required_version = ">= 1.0"
  required_providers {
    time = {
      source  = "hashicorp/time"
      version = ">= 0.7"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.13"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.0"
    }
  }
}

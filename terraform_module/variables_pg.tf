variable "pg_replicas" {
  type    = number
  default = 1
}
variable "pg_volume_size" {
  type    = string
  default = "1Gi"
}

variable "pg_backups_volume_enabled" {
  type    = bool
  default = false
}
variable "pg_backups_volume_size" {
  type    = string
  default = "10Gi"
}
variable "pg_backups_volume_full_schedule" {
  type    = string
  default = null
}
variable "pg_backups_volume_diff_schedule" {
  type    = string
  default = null
}
variable "pg_backups_volume_incr_schedule" {
  type    = string
  default = null
}
variable "pg_backups_volume_full_retention" {
  type    = number
  default = 9999999
}
variable "pg_backups_volume_full_retention_type" {
  type    = string
  default = "count"
}

variable "pg_backups_s3_enabled" {
  type    = bool
  default = false
}
variable "pg_backups_s3_bucket" {
  type    = string
  default = null
}
variable "pg_backups_s3_region" {
  type    = string
  default = null
}
variable "pg_backups_s3_endpoint" {
  type    = string
  default = null
  validation {
    condition     = var.pg_backups_s3_endpoint == null ? true : length(regexall("^https?://", var.pg_backups_s3_endpoint)) < 1
    error_message = "The endpoint should not contain the scheme nor the bucket name. It should be the generic S3 provider endpoint."
  }
}
variable "pg_backups_s3_access_key" {
  type      = string
  default   = null
  sensitive = true
}
variable "pg_backups_s3_secret_key" {
  type      = string
  default   = null
  sensitive = true
}
variable "pg_backups_s3_full_schedule" {
  type    = string
  default = null
}
variable "pg_backups_s3_diff_schedule" {
  type    = string
  default = null
}
variable "pg_backups_s3_incr_schedule" {
  type    = string
  default = null
}

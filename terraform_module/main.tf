resource "helm_release" "pgo" {
  chart           = "crunchydata-pgo-cluster"
  repository      = "https://gitlab.com/api/v4/projects/33954491/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    <<-EOT
      instance:
        resources:
          limits:
            memory: ${var.instance_quota.limits.memory}
            cpu: ${var.instance_quota.limits.cpu}
          requests:
            memory: ${var.instance_quota.requests.memory}
            cpu: ${var.instance_quota.requests.cpu}
        sidecars:
          replicaCertCopy:
            resources:
              limits:
                memory: ${var.instance_replicacert_quota.limits.memory}
                cpu: ${var.instance_replicacert_quota.limits.cpu}
              requests:
                memory: ${var.instance_replicacert_quota.requests.memory}
                cpu: ${var.instance_replicacert_quota.requests.cpu}
      backups:
        pgBackRest:
          repoHost:
            resources:
              limits:
                memory: ${var.backups_pgBackRest_repoHost_quota.limits.memory}
                cpu: ${var.backups_pgBackRest_repoHost_quota.limits.cpu}
              requests:
                memory: ${var.backups_pgBackRest_repoHost_quota.requests.memory}
                cpu: ${var.backups_pgBackRest_repoHost_quota.requests.cpu}
          sidecars:
            pgBackRest:
              resources:
                limits:
                  memory: ${var.backups_pgBackRest_sidecars_pgBackRest_quota.limits.memory}
                  cpu: ${var.backups_pgBackRest_sidecars_pgBackRest_quota.limits.cpu}
                requests:
                  memory: ${var.backups_pgBackRest_sidecars_pgBackRest_quota.requests.memory}
                  cpu: ${var.backups_pgBackRest_sidecars_pgBackRest_quota.requests.cpu}
          jobs:
            resources:
              limits:
                memory: ${var.backups_pgbackrest_jobs_quota.limits.memory}
                cpu: ${var.backups_pgbackrest_jobs_quota.limits.cpu}
              requests:
                memory: ${var.backups_pgbackrest_jobs_quota.requests.memory}
                cpu: ${var.backups_pgbackrest_jobs_quota.requests.cpu}

      %{~if var.pg_backups_volume_enabled || var.pg_backups_s3_enabled~}
      multiBackupRepos:
        %{~if var.pg_backups_volume_enabled~}
        - volume:
            backupsSize: ${var.pg_backups_volume_size}
          %{~if var.pg_backups_volume_full_schedule != null || var.pg_backups_volume_diff_schedule != null || var.pg_backups_volume_incr_schedule != null~}
          schedules:
            %{~if var.pg_backups_volume_full_schedule != null~}
            full: "${var.pg_backups_volume_full_schedule}"
            %{~endif~}
            %{~if var.pg_backups_volume_diff_schedule != null~}
            differential: "${var.pg_backups_volume_diff_schedule}"
            %{~endif~}
            %{~if var.pg_backups_volume_incr_schedule != null~}
            incremental: "${var.pg_backups_volume_incr_schedule}"
            %{~endif~}
          retentionFull:
            type: ${var.pg_backups_volume_full_retention_type}
            value: "${var.pg_backups_volume_full_retention}"
          %{~endif~}
        %{~endif~}
        %{~if var.pg_backups_s3_enabled~}
        - s3:
            bucket: ${var.pg_backups_s3_bucket}
            endpoint: ${var.pg_backups_s3_endpoint}
            region: ${var.pg_backups_s3_region}
            key: ${var.pg_backups_s3_access_key}
            keySecret: ${var.pg_backups_s3_secret_key}
          %{~if var.pg_backups_s3_full_schedule != null || var.pg_backups_s3_diff_schedule != null || var.pg_backups_s3_incr_schedule != null~}
          schedules:
            %{~if var.pg_backups_s3_full_schedule != null~}
            full: "${var.pg_backups_s3_full_schedule}"
            %{~endif~}
            %{~if var.pg_backups_s3_diff_schedule != null~}
            differential: "${var.pg_backups_s3_diff_schedule}"
            %{~endif~}
            %{~if var.pg_backups_s3_incr_schedule != null~}
            incremental: "${var.pg_backups_s3_incr_schedule}"
            %{~endif~}
          %{~endif~}
        %{~endif~}
      %{~endif~}
    EOT
  ], var.values)

  set {
    name  = "instance.replicaCount"
    value = var.pg_replicas
  }
  set {
    name  = "instance.size"
    value = var.pg_volume_size
  }
}

resource "time_sleep" "wait_5_seconds" {
  depends_on = [helm_release.pgo]

  create_duration = "5s"
}

locals {
  secret-name = "${var.chart_name}-pguser-${var.chart_name}"
}

data "kubernetes_secret" "pgo" {
  depends_on = [time_sleep.wait_5_seconds]
  metadata {
    name      = local.secret-name
    namespace = var.namespace
  }
}

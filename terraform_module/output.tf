output "dbname" {
  sensitive = true
  value     = data.kubernetes_secret.pgo.data.dbname
}
output "host" {
  sensitive = true
  value     = data.kubernetes_secret.pgo.data.host
}
output "jdbc-uri" {
  sensitive = true
  value     = data.kubernetes_secret.pgo.data.jdbc-uri
}
output "secret-key_password" {
  value = "password"
}
output "password" {
  sensitive = true
  value     = data.kubernetes_secret.pgo.data.password
}
output "port" {
  sensitive = true
  value     = data.kubernetes_secret.pgo.data.port
}
output "uri" {
  sensitive = true
  value     = data.kubernetes_secret.pgo.data.uri
}
output "secret-key_user" {
  value = "user"
}
output "user" {
  sensitive = true
  value     = data.kubernetes_secret.pgo.data.user
}
output "verifier" {
  sensitive = true
  value     = data.kubernetes_secret.pgo.data.verifier
}
output "secret-name" {
  value = local.secret-name
}

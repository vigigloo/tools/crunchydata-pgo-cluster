variable "namespace" {
  type = string
}

variable "chart_name" {
  type = string
}

variable "chart_version" {
  type    = string
  default = "0.5.0"
}

variable "values" {
  type    = list(string)
  default = []
}

# not sure how to properly handle this right now but I need a quick fix
# tflint-ignore: terraform_unused_declarations
variable "image_repository" {
  type    = string
  default = "registry.developers.crunchydata.com/crunchydata/postgres-operator"
}

# tflint-ignore: terraform_unused_declarations
variable "image_tag" {
  type    = string
  default = "ubi8-5.0.4-0"
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "instance_quota" {
  type = object({
    limits = object({
      cpu    = string
      memory = string
    })
    requests = object({
      cpu    = string
      memory = string
    })
  })
  default = {
    limits = {
      cpu    = "1000m"
      memory = "2048Mi"
    }
    requests = {
      cpu    = "500m"
      memory = "1024Mi"
    }
  }
}

variable "instance_replicacert_quota" {
  type = object({
    limits = object({
      cpu    = string
      memory = string
    })
    requests = object({
      cpu    = string
      memory = string
    })
  })
  default = {
    limits = {
      cpu    = "500m"
      memory = "512Mi"
    }
    requests = {
      cpu    = "10m"
      memory = "10Mi"
    }
  }
}

variable "backups_pgBackRest_repoHost_quota" {
  type = object({
    limits = object({
      cpu    = string
      memory = string
    })
    requests = object({
      cpu    = string
      memory = string
    })
  })
  default = {
    limits = {
      cpu    = "500m"
      memory = "512Mi"
    }
    requests = {
      cpu    = "20m"
      memory = "80Mi"
    }
  }
}

variable "backups_pgBackRest_sidecars_pgBackRest_quota" {
  type = object({
    limits = object({
      cpu    = string
      memory = string
    })
    requests = object({
      cpu    = string
      memory = string
    })
  })
  default = {
    limits = {
      cpu    = "500m"
      memory = "512Mi"
    }
    requests = {
      cpu    = "10m"
      memory = "300Mi"
    }
  }
}

variable "backups_pgbackrest_jobs_quota" {
  type = object({
    limits = object({
      cpu    = string
      memory = string
    })
    requests = object({
      cpu    = string
      memory = string
    })
  })
  default = {
    limits = {
      cpu    = "500m"
      memory = "512Mi"
    }
    requests = {
      cpu    = "250m"
      memory = "256Mi"
    }
  }
}

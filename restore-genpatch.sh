#!/bin/bash
set -Eeuo pipefail

PG_RESTORE_ANNOTATION=$(date -Iseconds)

cat << EOF > restore-patch.yaml
metadata:
  annotations:
    postgres-operator.crunchydata.com/pgbackrest-restore: "$PG_RESTORE_ANNOTATION"
spec:
  backups:
    pgbackrest:
      restore:
        enabled: true
        options:
EOF
if [ ! -z "${PG_RESTORE_DATE:-}" ]
then
  cat << EOF >> restore-patch.yaml
        - --type=time
        - --target="$PG_RESTORE_DATE"
EOF
else
  cat << EOF >> restore-patch.yaml
        - --set="$PG_RESTORE_SET"
EOF
fi
cat << EOF >> restore-patch.yaml
        repoName: $PG_RESTORE_REPO
        resources:
          limits:
            cpu: 1000m
            memory: 1024Mi
          requests:
            cpu: 500m
            memory: 512Mi
EOF
cat << EOF > restore-patch-end.yaml
metadata:
  annotations:
    postgres-operator.crunchydata.com/pgbackrest-restore: null
spec:
  backups:
    pgbackrest:
      restore: null
EOF

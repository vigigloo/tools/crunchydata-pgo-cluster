#!/bin/bash

function log {
    echo ProgressLog "$@" >> log.txt 2>&1
}
function execlog {
    eval "$@" >> log.txt 2>&1
}

images=$(cat images.txt)
old_registry="registry.developers.crunchydata.com"
for image in $images
do
    old_tag="$image"
    log Processing image: "$old_tag"
    if echo "$old_tag" | grep postgres-operator 1>/dev/null
    then
        new_registry="registry.gitlab.com/vigigloo/tools/crunchydata-pgo"
    else
        new_registry="registry.gitlab.com/vigigloo/tools/crunchydata-pgo-cluster"
    fi
    new_tag=$(echo "$old_tag" | sed "s|$old_registry|$new_registry|")
    log New tag is: "$new_tag"
    if execlog docker pull "$new_tag"
    then
        log Mirrored image found: "$new_tag"
    else
        log Mirrored image not found: "$new_tag"
        if execlog docker pull "$old_tag"
        then
            log Original image found: "$old_tag"
            log Retagging as: "$new_tag"
            execlog docker tag "$old_tag" "$new_tag"
            log Pushing: "$new_tag"
            execlog docker push "$new_tag"
        else
            log Original image not found: "$old_tag"
        fi
    fi
done


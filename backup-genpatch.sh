#!/bin/bash
set -Eeuo pipefail

PG_BACKUP_ANNOTATION=$(date -Iseconds)

cat << EOF > backup-patch.yaml
metadata:
  annotations:
    postgres-operator.crunchydata.com/pgbackrest-backup: "$PG_BACKUP_ANNOTATION"
spec:
  backups:
    pgbackrest:
      manual:
        options:
        - --type=$PG_BACKUP_TYPE
        repoName: $PG_BACKUP_REPO
EOF
cat << EOF > backup-patch-end.yaml
metadata:
  annotations:
    postgres-operator.crunchydata.com/pgbackrest-backup: null
spec:
  backups:
    pgbackrest:
      manual: null
EOF
